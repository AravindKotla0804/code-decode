package com.java;

import java.util.Scanner;

public class Message {

	public static void main(String[] args) {
		 int len,max,i;
		 Scanner sc = new Scanner(System.in);
		 System.out.println("Enter length of array");
		 len = sc.nextInt();
		 int a[] = new int[100];
		 System.out.println("Enter array values");
		 for(i=0;i<=len;i++) {
			 a[i]=sc.nextInt();
		 }
		 max=a[0];
		 for(i=0;i<=len;i++) {
			 if(max<a[i]) {
				 max=a[i];
			 }
		 }
		 System.out.println("The max array value is :" +max);
	}
}
