package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.customException.BussinessException;
import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;

@Service
public class EmployeeService  {

	@Autowired
	private EmployeeRepository employeeRepo;
	
	public Employee addEmployee(Employee employee) {
		return employeeRepo.save(employee);
	}

	public List<Employee> getAllEmployees() {
		
		try {
			List<Employee> empList = employeeRepo.findAll();
			if(empList.isEmpty())
				throw new BussinessException("602","Hey list completely empty, we have nothing to return");
			return empList;
		}
		catch(Exception e) {
			throw new BussinessException("603","something went wrong in service layer" +e.getMessage());
		}
		
	}

	public Employee getEmpById(Long empid) {
		
		return employeeRepo.findById(empid).get();
	}
	
	public void deleteEmpById(Long empid) {
		employeeRepo.deleteById(empid);
	}
	
}
